# Getting Started with Ms Forecast API

This project was bootstrapped with [express](https://github.com/facebook/create-react-app).

## Available Scripts

In the project directory, you can run:

### `DEBUG=forecast-back:* npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in your browser.

## launch Project

Install dependencies
### `npm install`
Create a .env file and the put your API_KEY
### `WEATHER_API_KEY=*****************`

Start App
### `DEBUG=forecast-back:* npm start`

## To test Endpoints

get cities "http://localhost:3000/weather/cities" ==> Post request

### `body exp {"city" : "Paris"}`

get cities "http://localhost:3000/weather/daily" ==> Post request

### `body exp {"lat": "51.5073219", "lon" : "0.1276474"}`