var express = require("express");
var router = express.Router();
var request = require("sync-request");
var NodeCache = require("node-cache");
require('dotenv').config()

const Cache = new NodeCache();
const part = "hourly,alerts";

const WEATHER_API_KEY = process.env.WEATHER_API_KEY;

/*----  cities listing.---- */
router.post("/cities", function (req, res) {

  const city = req.body.city;
  if (Cache.has("uniqueKey")) {
    res.send(Cache.get("uniqueKey"));
  }
  const url = `http://api.openweathermap.org/geo/1.0/direct?q=${city}&limit=${7}&appid=${WEATHER_API_KEY}`;
  const cities = request("GET", url);

  const data = JSON.parse(cities.getBody("utf8"));
  Cache.set("uniqueKey", {
    longitude: data[0].lat,
    name: data[0].name,
    latitude: data[0].lon
  });
  res.send({
    longitude: data[0].lat,
    name: data[0].name,
    latitude: data[0].lon
  });
});
/*---  daily forcasts ---- */
router.post("/daily", function (req, res) {
  const lat = req.body.lat;
  const lon = req.body.lon;
  if (Cache.has("uniqueKey2")) {
    res.send(Cache.get("uniqueKey2"));
  }
  let url = `https://api.openweathermap.org/data/2.5/onecall?lat=${lat}&lon=${lon}&exclude=${part}&appid=${WEATHER_API_KEY}`;
  const results = request("GET", url);
  const data = JSON.parse(results.getBody("utf8"));
  Cache.set("uniqueKey2", data);
  res.json(data);
});

module.exports = router;
