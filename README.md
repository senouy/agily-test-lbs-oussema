### Temps de réalisation ?

    -FRONT : 3 h 30
    -BACK : 2 h 30

### Quels sont les principes que tu as appliqué ?

J'essaye toujours d'appliquer les principes SOLID dans la mesure du possible. 
Sur ce mini projet, j'ai surtout insisté sur le S (single responsability)

### Peux-tu expliquer les décisions que tu as prise et pourquoi c'est la meilleure approche ?

J'ai hésité entre utiliser creact react app ou ajouter une surcouche avec next.js mais j'ai estimé qu'il était pas nécessaire d'ajouter la partie SSR, le projet n'ayant pas pour objectif d'être référencé. 
Si besoin, j'aurais plutôt ajouté un la lib react-helmet pour ajouter des meta data

### Quelles sont tes recommandations pour un travail futur ?

Il faudrait appeller les api du backend au lieu de faire les appels directement du front

Il faudrait aussi gérer les erreurs (Back , Front) et ajouter des tests unitaires

Migration vers Typescript pour notamment gérer le typage statique
