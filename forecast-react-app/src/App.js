import React from "react";
import "./App.css";
import SearchBox from "./components/SearchBox";

function App() {

  return (
    <div className="App">
      <div className="container">
        <h1
          style={{
            fontSize: "50px",
          }}
          className="title"
        >
          The Forecast <br /> Weather App
        </h1>
        <SearchBox />
      </div>
    </div>
  );
}

export default App;
