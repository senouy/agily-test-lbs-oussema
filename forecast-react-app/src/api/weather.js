const baseUrl = "http://api.openweathermap.org/geo/1.0";
const baseUrlForecast = "https://api.openweathermap.org/data/2.5";

export const fetchCities = async (city, limit) => {
  let url = `${baseUrl}/direct?q=${city}&limit=${limit}&appid=${process.env.REACT_APP_WEATHER_API_KEY}`;

  return (await fetch(url)).json();
};

export const fetchExtendedForecastData = async (lat, lon, part) => {
  let url = `${baseUrlForecast}/onecall?lat=${lat}&lon=${lon}&exclude=${part}&appid=${process.env.REACT_APP_WEATHER_API_KEY}`;

  return (await fetch(url)).json();
};
