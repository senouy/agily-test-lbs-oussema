import React from "react";
import { formatDate } from "../../utils/functions";
import "./styles.css";

const CurrentWeather = ({
  tempJour,
  tempNuit,
  humidity,
  pressure,
  wind_speed,
}) => {
  const { weekday, daynumber, month } = formatDate(Date.now());

  return (
    <div className="SelectedMeteoDay">
      <div className="meteoTitle">
        <img src="/icons/sun.svg" alt="weather" height="40" width="40" />
        <h2 className="selectedDay">
          {weekday} {daynumber} {month}
        </h2>
      </div>
      <div className="meteoDescription">
        <div className="details">
          <p>Jour - {tempJour}°C</p>
          <p>Nuit - {tempNuit}°C</p>
          <p>Humidité - {humidity}%</p>
        </div>
        <div className="details">
          <p>Pression - {pressure}hPa</p>
          <p>Vent - {wind_speed} km</p>
        </div>
      </div>
    </div>
  );
};

export default CurrentWeather;
