import React, { useEffect, useState } from "react";
import "./styles.css";

import { Link, useParams } from "react-router-dom";
import { fetchExtendedForecastData } from "../../api/weather";
import CurrentWeather from "./CurrentWeather";
import ForecastItem from "./ForecastItem";
import { getNextSevenDays } from "../../utils/functions";

const Forecast = () => {
  const [dailyData, setDailyData] = useState();
  const [currentWeather, setCurrentWeather] = useState();

  const { lat, lon } = useParams();

  useEffect(() => {
    fetchExtendedForecastData(lat, lon, "minutely,hourly,alerts").then(
      (res) => {
        setDailyData(res?.daily);
        setCurrentWeather(res?.current);
      }
    );
  }, [lat, lon]);
  return (
    <div
      className="container_page"
      style={{
        backgroundImage: `url(/tourEiffel.jpeg)`,
      }}
    >
      <div className="wrapper">
        <div className="firstRow">
          <div className="GoBack">
            <Link to={"/"}>
              <img
                src="/icons/flèche-gauche.png"
                alt=""
                height="20"
                width="20"
              />
            </Link>
          </div>
          ​
          <div className="ListOfDays">
            {dailyData &&
              dailyData.map((item, index) => (
                <ForecastItem
                  key={index}
                  temp={item.temp.day}
                  icon={item.weather[0].icon}
                  date={item.dt}
                  day={getNextSevenDays()[index].day}
                  weekday={getNextSevenDays()[index].weekday}
                  month={getNextSevenDays()[index].month}
                />
              ))}
          </div>
        </div>
        {currentWeather && (
          <CurrentWeather
            tempJour={currentWeather.temp}
            tempNuit={currentWeather.temp}
            humidity={currentWeather.humidity}
            pressure={currentWeather.pressure}
            wind_speed={currentWeather.wind_speed}
          />
        )}
      </div>
    </div>
  );
};

export default Forecast;
