import React from "react";
import "./styles.css";

const ForecastItem = ({ temp, icon, day , weekday , month }) => {
  return (
    <div className="days">
      <img
        src={`https://openweathermap.org/img/wn/${icon}@4x.png`}
        alt="weather"
        height="30"
        width="30"
      />
      <div className="date">
        <h4>{day}</h4>
        <p className="dateMois">
          {weekday} {month}
        </p>
      </div>
      <h2>{temp}°C</h2>
    </div>
  );
};

export default ForecastItem;
