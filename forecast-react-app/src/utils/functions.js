export function formatDate(timestamp) {
  const date = new Date(timestamp);
  const month = date.toLocaleString("fr-FR", { month: "long" });
  const weekday = date.toLocaleString("fr-FR", { weekday: "long" });
  const daynumber = date.getDate();
  return { weekday, daynumber, month };
}

export function getNextSevenDays() {
  const days = [
    "Dim",
    "Lundi",
    "Mardi",
    "Mercredi",
    "Jeudi",
    "Vendredi",
    "Samedi",
  ];

  const next7Days = [];
  for (let i = 0; i < 8; i++) {
    const weekday = new Date(
      Date.now() + (i + 1) * 24 * 60 * 60 * 1000
    ).getDate();

    const month = new Date(
      Date.now() + (i + 1) * 24 * 60 * 60 * 1000
    ).toLocaleString("fr-FR", { month: "long" });
    
    next7Days.push({
      day: days[new Date(Date.now() + (i + 1) * 24 * 60 * 60 * 1000).getDay()],
      weekday: weekday,
      month: month,
    });
  }
  return next7Days;
}
